import { Component, OnInit } from '@angular/core';

import { Ingredient } from '../models/ingredient.model';

import { ShoppingListService } from './shopping-list.service';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.scss'],
})
export class ShoppingListComponent implements OnInit {
  public ingredients: Ingredient[];

  constructor( public slSerivce: ShoppingListService) { }

  ngOnInit(): void {
    this.ingredients = this.slSerivce.getIngredients();
    this.slSerivce.ingredientsChanged
      .subscribe((ingredients: Ingredient[]) => this.ingredients = ingredients);
  }
}
