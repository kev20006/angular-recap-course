import { Component, Input, OnInit } from '@angular/core';
import { Ingredient } from 'src/app/models/ingredient.model';
import { RecipeService } from '../../recipe.service';

@Component({
  selector: 'app-recipe-item',
  templateUrl: './recipe-item.component.html',
  styleUrls: ['./recipe-item.component.scss']
})
export class RecipeItemComponent implements OnInit {
  @Input() public name: string;
  @Input() public description: string;
  @Input() public imgPath: string;
  @Input() public ingredients: Ingredient[];


  constructor(public recipeService: RecipeService) { }

  ngOnInit(): void {
  }

  onSelected(): void {
    this.recipeService.recipeSelected.emit({
      name: this.name,
      description: this.description,
      imgPath: this.imgPath,
      ingredients: this.ingredients
    });
  }

}
